# __PROBLEM 6__

### Video Notes
- There is a natural human tendency to start at a point that feels lke the beginning/origin (regarding searching)
- It's worth it co continue alternatives
- Regarding Problem 6, it builds on Problem 5 with iteration moving along row and column ahead of time
    - Now need to decide on what location to check -> (1) start reading matrix -> (2) start at top left
    - Alt(?) : Can we start at some other position for an efficient search

### Prompt
- Given a matrix that is monotonically increasing along rows and columns, as well as target value k
- Return true if value exists in matrix, false otherwise
    ```js
        []function findMonontic(matrix, k){
            // code
        }
    ```
### Coding Assumption
```js

```
### Initial Thought Process

#### Python Implementation
- [Python Solution](problem5.py)

##### My Solution
- [Problem 5 Implementation](solution.js)
