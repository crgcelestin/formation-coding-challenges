# __PROBLEM 5__

### Video Notes
- In tackling problems, ask 2 things: what are knowns, unknowns?
    - For this problem, each cell to right and bottom must be equal or greater in value
- Find steps that are to be repeated, look at cell and what pattern should be existent throughout

### Prompt
- Given a rectangular 2d array of integers, return true
if all rows, columns are monotonically increasing
- Every successive value along rows, columns are at least as large as what cam prior

### Coding Assumption
```js
arr = [[0, 0, 0, 1],
       [1, 1, 1, 2],
       [2, 3, 4, 5]]
// returns true as each successive element in each row is => prior element

arr = [[0, 0, 0, 1],
       [1, 1, 3, 2],
       [2, 3, 4, 5]]
// returns false as the second row violates with a 2 following a 3

function isMatrixMonotonic(matrix){
    // code
}
```

### Initial Thought Process
- iterate through arrays, then iterate through subarrays
- ensure that during iteration each successive element is greater or equal to (easy for each element in rows)
- for columns, during iteration check successive elements as well as directly downward if still rows below

#### Python Implementation
- [Python Solution](problem5.py)

##### My Solution
- [Problem 5 Implementation](solution.js)
