function isMatrixMonotonic(matrix) {
    const m_length = matrix.length
    for (let r = 0; r < m_length; r++) {
        for (let c = 0; c < matrix[r].length; c++) {
            let value = matrix[r][c]
            if (
                r > 0 && matrix[r - 1][c] > value
            ) {
                return false
            }
            if (
                c > 0 && matrix[r][c - 1] > value
            ) {
                return false
            }

        }
    }
    return true
}
const monton_matrix = [
    [0, 0, 0, 1],
    [1, 2, 2, 2],
    [2, 3, 3, 3]
]
console.log(
    isMatrixMonotonic(
        monton_matrix
    )
)

const notMonton_matrix = [
    [0, 0, 0, 1],
    [1, 1, 3, 2],
    [2, 3, 4, 5]
]
console.log(
    isMatrixMonotonic(
        notMonton_matrix
    )
)
