class Solution:
    def isMatrixMonotonic(matrix: list[list[int]]):
        # iterate throughout matrix
        for r in range(len(matrix)):
            for c in range(len(matrix[r])):
                value=matrix[r][c]
                # check for 2 edge cases
                # first being 1 row up
                if(
                    r>0 and matrix[r-1][c]>value
                ):
                    return False
                if(
                    c>0 and matrix[r][c-1]>value
                ):
                    return False
        return True
