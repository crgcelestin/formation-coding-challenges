function isMatrixMonotonic(matrix) {
    // two loops = typical, row-major traversal
    for (let r = 0; r < matrix.length; r++) {
        for (let c = 0; c < matrix[r].length; c++) {
            // get location value
            const value = matrix[r][c]
            // if on row pass first, look up 1 pos and make sure value is not larger
            if (
                r > 0 && matrix[r - 1][c] > value
            ) {
                return false
            }
            // if on column pass first, look to position at left and make sure value is not larger
            if (
                c > 0 && matrix[r][c - 1] > value
            ) {
                return false
            }
        }
    }
    return true
}
