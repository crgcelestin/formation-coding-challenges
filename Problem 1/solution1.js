class LLNode {
    constructor(value, next = null) {
        this.value = value;
        this.next = next;
    }
}
function deleteMiddleNode(head) {
    /*
        LL problems that modify list can be made simpler with temp insertion of node as head
    */
    let tempHead = new LLNode(Infinity, head)
    /*
        solution utilizes slow/fast pattern where fast===2*(slow)
    */
    let slow = tempHead;
    let fast = head?.next;
    while (fast) {
        //slow moves ahead one
        slow = slow.next;
        //fast moves ahead two
        fast = fast.next?.next;
    }
    /*
        (1) Fast has moved to end (now null), slow is at middle
        (2) update slow pointer's next to skip node following it
    */
    slow.next = slow.next ? slow.next.next : null;
    // return real head, not temp head
    return tempHead.next;
}

/*
    Explanation:
    (1) Initialize LinkList Node class
    (2) create deleteMiddleNode function in linkedlistNode class
    (3) set a var that is to store new nodes (let tempHead = new LLNode(head))
    (4) set a slow var equal to temporary head (slow=tempHead) with a fast variable that is to traverse the linked list with 2x speed (fast=head?.next) - uses ternary operator to either assign head.next or head (if head.next is null)
    (5) with fast now at end due to while loop and slow now at middle due to its 1/2 speed (use while fast loop, move slow to next and fast continues to move to next node until reaching null)
    (6) as we know slow is now the middle node and fast is === end, set slow to the node its linked to following or just make it null if there are no other nodes maybe in the case of a 2 node LL -> so either skip to next node or sent it to null if there is no next
    (broke out of while loop and now can skip middle)

    Tc: O(N), space complexity: O(1)
*/
