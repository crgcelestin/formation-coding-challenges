class LLNode {
    constructor(value, next = null) {
        this.value = value;
        this.next = next;
    }
}
/*
    implementation with linkedlist class
    deleteMiddleNode being a instance method
    of a linkedlist instance
*/
class linkedlist {
    constructor() {
        this.head = null;
        this.tail = null;
        this.length = 0;
    }
    push(val) {
        var newNode = new Node(val)
        this.length += 1
        if (!this.head) {
            this.head = newNode
            this.tail = this.head
        } else {
            this.tail.next = newNode
            this.tail = newNode;
        }
        return this
    }
    deleteMiddleNode(head) {
        let tempHead = this.head
        let slow = tempHead;
        let fast = head?.next;
        while (fast) {
            slow = slow.next;
            fast = fast.next?.next;
        }
        slow.next = slow.next ? slow.next.next : null;
        return tempHead.next;
    }
}
