class LLNode:
    def __init__(self, value, link=None):
        self.value = value
        self.link = link


class LinkedList:
    def __init__(self, tail=None, head=None, length=0):
        self.tail = tail
        self.head = head
        self.length = length
    def traverse(self):
      if not self.length: return
      current=self.head
      count=self.length
      nodes=[]
      while(current and count):
        nodes.append(current.value)
        count-=1
        current=current.link
      return nodes
    def push(self, val):
        newNode = LLNode(val)
        if(self.length==0):
          self.head=newNode
          self.tail=newNode
        else:
          self.tail.link=newNode
          self.tail=newNode
        self.length+=1
        return self.traverse()
    def pop(self):
      if self.length==0: return
      current=self.head
      if self.length==1:
        self.head=None
        self.tail=None
      else:
        while(current and current.link.link):
            current=current.link
        self.tail=current
        current.link=None
        self.length-=1
        return self.traverse()
    def deleteMiddleNode(self):
      if not self.length: return
      current=self.head
      middle=abs(self.length/2)
      while(current and middle>1):
        current=current.link
        middle-=1
      # skip middle, by updating node prior to middle to have its link pointer point to the node after the next node
      current.link=current.link.link
      self.length-=1
      return self.traverse()

LL = LinkedList()
LL.push(1)
LL.push(2)
LL.push(3)
LL.push(4)
