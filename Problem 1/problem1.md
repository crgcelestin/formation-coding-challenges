# __PROBLEM 1__

## Prompt
- Given linked list, delete middle node
- If list is even length, delete node at start of 2nd half of list

## Intro Video Notes
- Even the simplest problems can first involve an initial thought process
- Think about potential edge cases and potential bugs upfront by having plan to write code
- Few minutes upfront allow for debugging


## Image
![Prompt Image](Images/prompt1.png)

## Coding Assumption
```javascript
    // You may assume the node class is:
     class LLNode {
       constructor(value, next = null) {
         this.value = value;
         this.next = next;
       }
     }
    function deleteMiddleNode(head) {
     /* your code here */
    }
```

## Initial Thought Process
- For this problem, which in my own terms is, delete the node where the length property/counter is equal to half the length.
    * First check edge cases which are when a lin
    * I want to first find the length of whatever node is passed in
    * Then I want to traverse the node until that node is reached
    * Then when I do reach node were

### First Thoughts At Implementation
```javascript
//First thoughts at implementation
// I found this to be quite erroneous but allowed me to get ideas out
//with the delete method being the proper implemented helper/class method for a particular instance
if(!list.length || list.length===1) return list;
delete(LL, index){
    //delete node and corresponding links
}
var middle;
//even list uses middle index
if(list.length%2===0){
    middle=list.length/2
}else{
    //odd length list uses index rounded down
    middle=Math.floor(list.length/2)
}
let i=0;
while(i<middle){
    if(i===middle){
        delete(Node) //Node.pop?
        //link node before deleted middle to node after deleted middle
    }
    i++
}
return LL
```

### Reviewing Prior First Impressions of Implementation
```javascript
    /*
        On second thought, this implementation can be fully explored and corrected
        given the head parameter which contains the head node and what I am assuming is all the nodes after (start of LL)
        we can then store it in a var called pointer and use a counter (i) to store length
    */
    delete middleNode(head){
    let pointer = new LLNode(head)
    let i = 0;
    if (pointer.value) {
        let nextNode = pointer.next
        if (nextNode) {
            i++
            nextNode = nextNode.next
        }
    }
    /*
      initialize count var to 0,
      in order to get to node at 1/2 length create while loop with condition of count being less than i
      continue to move to next node
      calculate middle (2 opts if even or odd)
      if count reaches middle then set current node 'nextNode' to the node after it or null
      else increment count and go to next Node
    */
    let count = 0;
    while (count < i) {
        let nextNode = pointer.next
        if (i % 2 == 0) var middle = i / 2;
        else var middle = Math.floor(i / 2);
        if (count === middle) {
            nextNode = nextNode ? nextNode.next : null;
        } else {
            nextNode;
            count++
        }
        }
    }
```

### My Implementation
- [Problem 1 Implementation](problem1.js)
### Solution
- [Problem 1 Official Solution](solution1.js)
