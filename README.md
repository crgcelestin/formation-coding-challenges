# Formation 21 Day Coding Challenge
 - 21 challenges from Formation.dev

## Link to Challenge
[Formation Coding Challenge](https://gitlab.com/crgcelestin/formation-coding-challenges#:~:text=https%3A//d11cm304.na1,n6c8SsY6tW3N6rDl34S7kPW17_xPY6TbZB0W61VFJZ2KRnwHW1336R35F9KjPW9bj_wD6Y%2DFSmW8PktYZ68W2lxW6zv_Rz7RLngdW5B09r%2D1Lt2DHW99dklm38ngkrW56hR4Z7k6vtRW515y_d29KRWmW6N3Ss293fBx9W5GBrzx8FKbYcW86Rcbj6Pl03cN62pjsCjnDRpW30W9ty1Ntfz73lm71)

Problem | Python Implementation | Actual Solution (JS)| JavaScript Implementation (My Attempt) | Date | Status |
| --- | --- | --- | --- |--- | --- |
[Problem 1](/Problem%201/problem1.md) |[Problem 1](/Problem%201/problem1.py)  | [Solution 1](/Problem%201/solution1.js) | [Problem 1](/Problem%201/problem1.js) | 2023-03-27 | ⭕ Complete
[Problem 2](/Problem%202/problem2.md) |[Problem 2](/Problem%202/problem2.py)  | [Solution 2](/Problem%202/solution2.js) | [Problem 2](/Problem%202/problem2.js) | 2023-04-01 | ⭕ Complete|
| [Problem 3](/Problem%203/problem3.md)| [Problem 3](/Problem%203/problem3.py) |[Solution 3](/Problem%203/FormationSolution.js) | [Problem 3](/Problem%203/problem3.js)| 2023-04-12 | ⭕ Complete |
| [Problem 4](/Problem%204/problem4.md)|[Problem 4](/Problem%204/problem4.py)| [Solution 4](/Problem%204/solution.js) | [Problem 4](/Problem%204/problem4.js)| 2023-06-23 | ⭕ Complete |
[Problem 5](/Problem%205/problem5.md) | [Problem5](/Problem%205/problem5.py) | [Solution 5](/Problem%205/solution.js) | [Problem 4](/Problem%205/problem5.js) | 2023-06-24 | ⭕ Complete |
