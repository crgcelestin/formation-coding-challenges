/**
 *
 * @param {number[][]} matrix
 * @returns {boolean}
 */
function isToeplitz(matrix) {
    let matrix_length = matrix.length
    for (let r = 0; r < matrix_length; r++) {
        for (let c = 0; c < (matrix[r]).length; c++) {
            let value = matrix[r][c]
            if (r > 0 && c > 0
                && matrix[r - 1][c - 1] !== value) {
                return false
            }
        }
    }
    return true
}
const input1 = [
    [1, 2, 3, 4],
    [5, 1, 2, 3],
    [6, 5, 1, 2],
    [7, 6, 5, 1],
]
console.log(
    isToeplitz(
        input1
    )
)
const input2 = [
    [0, 0, 0, 0],
    [1, 1, 1, 1],
    [2, 2, 2, 2],
    [3, 3, 3, 3]
]
console.log(
    isToeplitz(
        input2
    )
)
