# python implementation
class ToeplitzSolution:
    def isToeplitz(matrix: list[list[int]]):
        length=len(matrix)
        for r in range(length):
          for c in range(len(matrix[r])):
                value=matrix[r][c]
                if(r>0 and c>0 and matrix[r-1][c-1]!=value):
                    return False
        return True

print(ToeplitzSolution.isToeplitz(
     [
        [1, 2, 3, 4],
        [5, 1, 2, 3],
        [6, 5, 1, 2],
        [7, 6, 5, 1],
    ]
)==True)
