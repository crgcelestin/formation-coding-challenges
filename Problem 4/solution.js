/*
    Natural human method is to scan all diagonals
*/

function isToeplitz(matrix) {
    //row-major traversal w/ 2 loops
    let len = matrix.length
    for (let r = 0; r < len; r++) {
        for (let c = 0; c < matrix[r].length; c++) {
            const value = matrix[r][c]
            // loop up and to left at a position diagonal from current. Only perform if we aren't indexing outside of matrix
            //check if value isn't same as current
            if (
                r > 0 && c > 0
                && matrix[r - 1][c - 1] !== value
            ) {
                return false
            }
        }
    }
    return true;
}
