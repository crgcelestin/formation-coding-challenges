# __PROBLEM 4__

## Video Notes
- There is a natural way for humans to understand tasks manually and doing so provides a great method for understanding a problem prior to conceiving algorithm or writing code, but can lead to traps
- Break from human tendencies and create an algo that can be cleanly expressed
- Think about minimal amount of work required to verify requirements of problem
## Prompt
- Toeplitz Matrix is a type of matrix where every value on every diagonal is the same
    - 'every value on every diagonal is the same'
        - From top left to bottom right, when you look at each diagonal starting from corner, each value is same as is with the array below in the coding assumption

-   Given a 2d matrix (multiDimensional array), return true if input is a Toeplitz matrix, false otherwise

### Coding Assumption
```javascript
   [
        [1 2 3 4],
        [5 1 2 3],
        [6 5 1 2],
        [7 6 5 1],
    ]
    /*
      [7][6][5][1][2][3][4]
         [6][5][1][2][3]
           [5][1][2]
              [1]
    shows each value is the same
    The above matrix would return true as it is a Toeplitz matrix
    */
function isToeplitz(m) {
  /* your code here */
}
```
### Initial Thought Process
- Start with implementing the traversal of matrix elements
   row -> column
- initialize value var that is the default element matrix[r][c] for initial matrix position (current)
- if r and c are not the first element and the value of the previous matrix position i.e the prior element in a given diagonal are not equal to that of current
  return false meaning it is not a toeplitz matrix
- if we never encounter that condition then that means each diagonal checks out and that it is a toeplitz

#### Python Implementation
- [Python Solution](problem4.py)
##### Solution
- [Problem 4 Implementation](solution4.js)
