class Node {
    constructor(value) {
        this.value = value;
        this.next = null;
    }
}
class LinkedList {
    constructor() {
        this.tail = null;
        this.head = null;
        this.size = 0;
    }

    traverse() {
        if (!this.size) return;
        let current = this.head;
        while (current) {
            console.log(current.value);
            current = current.next;
        }
        return 'done'
    }

    push(val) {
        const newNode = new Node(val);
        if (!this.size) {
            this.tail = newNode;
            this.head = newNode;
        } else {
            this.tail.next = newNode;
            this.tail = newNode;
        }
        this.size += 1;
    }

    reverse() {
        let current = this.head;
        this.head = this.tail;
        this.tail = current;
        let next;
        let prev = null;
        for (let i = 0; i < this.size; i++) {
            next = current.next;
            current.next = prev;
            prev = current;
            current = next;
        }
        return this;
    }
    reverseinGroupsOfK(k) {
        if (!this.head) return;
        // current points to current node in iteration, starting from LL head
        let current = this.head
        // prevTail keep track of tail node of prev group of reversed nodes
        let prevTail = null;
        //stores new head of reverse linked list
        let newHead = null;
        //iterate through list and reverse each group of k nodes
        while (current) {
            // tail node of current group of nodes to be reversed
            let tail = current;
            // used to reverse nodes
            let prev = null;
            // keep track of number of nodes reversed
            let count = 0;
            //reverse current k nodes group
            while (current && count < k) {
                // next stores node after current
                let next = current.next
                // pointer that points to next pointer now points to prev
                current.next = prev
                // prev is now current
                prev = current;
                // current is now the next node
                current = next;
                //increment count
                count++
            }
            //link current group of nodes to previous group
            // if true, there is a previous group of reversed nodes
            if (prevTail) {
                // if there is a previous group, set the next pointer to be the new head of current group
                prevTail.next = prev
            } else {
                // if no previous group, set newHead to prev (entire reverse linked list)
                newHead = prev;
            }
            // update prevtail to be tail node of current group
            prevTail = tail;
        }
        //update head of linkedlist
        this.head = newHead
        return this
        /*
            before
            list=[1,2,3,..]
            after
            list=[2,1,3,...]
        */
    }
}
