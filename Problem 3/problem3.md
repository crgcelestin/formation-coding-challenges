# __PROBLEM 3__

## Intro Video Notes
- LLs are worth practicing as they allow for practice with pointer manipulations
- Exercise req care regarding not corrupting list with pointer rewiring
- Reverse in blocks of nodes

# Prompt
- Given an LL 'assuming it is a singly Linked List' (as we know a SLL has nodes with only next pointer), reverse groups of k nodes
- Node groups will remain in order
- perform operation with k=2 on provided list:
    * 1 -> 2 -> 3 -> 4 -> 5
- with result being:
    * 2 -> 1 -> 4 -> 3 -> 5
- Performed in place with the next pointers in nodes and return new head
- Head param = list of some length (possibly 0), k = positive integer

## Image
![Problem 3 Image](Images/prompt3.png)

## Coding Assumption
```javascript
    class LLNode{
        constructor(value, next=null){
            this.value=value;
            this.next=next;
        }
    }
    function reverseInGroupsOfK(head,k){
        //code
    }
```
## Initial Thought Process

### Java Script Reverse LL
```javascript
class Node{
    constructor(value, next=null){
        this.value=value
        this.next=next
    }
}

class LinkedList{
    constructor(){
        this.head=null
        this.tail=null
        this.length=0
    }
    push(value){
        var newNode=new Node(value)
        if(!this.length){
            this.head=newNode
            this.tail=this.head
        }else{
            this.tail.next=newNode
            this.tail=this.tail.next
        }
        this.length+=1
        return this
    }
    reverse(){
        var current=this.head
        this.head=this.tail
        this.tail=current
        var next;
        var prev=null
        for(i=0;i<this.length;i++){
            next=current.next
            current.next=prev
            prev=current
            current=next
        }
    }
}
```

### Attempt at Reverse with K Nodes
```javascript
    class Node{
        constructor(val){
            this.val=val
            this.next=null
        }
    }
    class LinkedList{
        constructor(){
            this.last=null
            this.next=null
            this.size=0;
        }
    }
    //assuming method for pushing into LL
    //assuming method for traversing LL
    reverseinKGroups(k){
        //in order to to traverse and reverse k nodes throughout provided LL
        if(!this.head) return;
        let current=this.head
        let prevT=null;
        let nHead=null;
        while(current){
            let tail=current;
            let prev=null;
            let count=0;
            while(current&&count<k){
                let next=current.next
                current.next=prev
                prev=current
                current=next
                count++
            }
            if(prevT){
                prevT.next=prev
            }else{
                nHead=prev
            }
            prevT=tail;
        }
        this.head=nHead
        return this
    }
```

### Python Implementation
- [Python Solution](problem3.py)
### Javascript Implementation
#### My Solution
- [Problem 3 Implementation](solution3.js)
#### Alt Solutions
- [Problem 3 Official Solution](FormationSolution.js)
- [Leetcode Solution](LeetCodeSolution.js)
