class LLNode:
    def __init__(self, value, link=None):
        self.value = value
        self.link = link


class LinkedList:
    def __init__(self, tail=None, head=None, length=0):
        self.tail = tail
        self.head = head
        self.length = length
    def traverse(self):
      if not self.length: return
      current=self.head
      count=self.length
      nodes=[]
      while(current and count):
        nodes.append(current.value)
        count-=1
        current=current.link
      return nodes
    def push(self, val):
        newNode = LLNode(val)
        if(self.length==0):
          self.head=newNode
          self.tail=newNode
        else:
          self.tail.link=newNode
          self.tail=newNode
        self.length+=1
        return self.traverse()
    def reverse(self):
        if not self.length: return
        current=self.head
        self.head=self.tail
        self.tail=current
        prev=None
        while(current):
          next = current.link
          current.link = prev
          prev = current
          current = next
        return self.traverse()
    def reverse_in_Knodes(self, k):
      if not self.length: return
      current=self.head
      count=0
      prevTail=None
      newHead=None
      while(current):
        tail=current
        count=0
        prev=None
        while(current and count<k):
          next=current.link
          current.link=prev
          prev=current
          current=next
          count+=1
        if prevTail:
          prevTail.link=prev
        else:
          newHead=prev
        prevTail=tail
      self.head=newHead
      return self.traverse()


LL = LinkedList()
LL.push(1)
LL.push(2)
LL.push(3)
LL.push(4)
