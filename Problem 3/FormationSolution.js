/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @param {number} k
 * @return {ListNode}
 */

/*
refactoring of formation's code
found that their provided solution was unable to handle
[1,2,3,4,5]
Formation Output: [3,2,1,5,4]
Correct: [3,2,1,4,5]
*/
var reverseKGroup = function (head, k) {
    //lists with 0 and 1 as length, don't require reversing
    //return unchanged
    if (!head || !head.next || k === 1) return head;

    //store current pointer at head
    //store a counter in order to restrain reversing to k
    let current = head;
    let count = 0;

    // Find the kth node from the current node
    while (current && count < k) {
        current = current.next;
        count++;
    }

    /*
        ex: if count increments and stops at k-1 (k being number of nodes to be reversed) then those k-1 nodes should not be reversed and should be returned in current order
    */
    if (count < k) {
        return head;
    }

    // Reverse the next group of k nodes recursively and get the new head
    let newHead = reverseKGroup(current, k);

    // Reverse the current group of k nodes
    /*
    head var that is returned is used for the actual reversal of k nodes implementation
    */
    let prev = null;
    let curr = head;
    while (count > 0) {
        let temp = curr.next;
        curr.next = prev;
        prev = curr;
        curr = temp;
        count--;
    }

    // Set the tail of the current group to point to the head of the next group
    head.next = newHead;

    // Return the new head of the current group
    return prev;
};

/*

*/
