def traversal(matrix):
    output = []
    if len(matrix) == 0:
        return output
    rstart = 0
    rend = len(matrix) - 1
    cstart = 0
    cend = len(matrix[0]) - 1
    direction = 0
    while rstart <= rend and cstart <= cend:
        # left to right of columns
        if direction == 0:
            for i in range(cstart, cend + 1, 1):
                output.append(matrix[rstart][i])
            rstart += 1
            direction = (direction + 1) % 4
        # down rows
        elif direction == 1:
            for i in range(rstart, rend + 1, 1):
                output.append(matrix[i][cend])
            cend -= 1
            direction = (direction + 1) % 4
        # right to left of columns
        elif direction == 2:
            # going from last column back to first column (traverse columns going from end)
            for i in range(cend, cstart - 1, -1):
                output.append(matrix[rend][i])
            # done with row so decrement
            direction = (direction + 1) % 4
            rend -= 1
        # #up rows
        # else:
        else:
            for i in range(rend, rstart - 1, -1):
                output.append(matrix[i][cstart])
            direction = (direction + 1) % 4
            cstart += 1
    return output


print(
    traversal(
        [
            [1, 2, 3, 4],
            [5, 6, 7, 8],
            [9, 10, 11, 12],
            [13, 14, 15, 16],
        ]
    )
)
