# __PROBLEM 2__

## Prompt
- Given a 2D rectangular matrix, return all of the values in a single, linear array in spiral order.

- Start at (0, 0) and first include everything in the first row.

- Then down the last column, back the last row (in reverse), and finally up the first column before turning right and continuing into the interior of the matrix.

## Image
![Prompt 2](Images/prompt2.png)

## Coding Assumption
```javascript
function spiralTraversal(matrix) {
  /* your code here */
}
```

## Initial Thought Process
- (1) Check edge case i.e is matrix empty or not, if empty return [];
- (2) Initialize starting point (0,0), end point (max x, max y) of matrix
- (3) We know that we have to move in 4 directions in a given a matrix larger than 1X1
    - Right to left will be 0, down will be 1, left to right will be 2, up will be 3
    - A direction variable will keep track starting at 0
    that will be incremented by 1
    - In order to make sure direction doesnt get greater than 3, we will use a formula to set it back within the confines of 0 and 3
    ```py3
        direction = (direction+1)%4
    ```
- (4) Create while loop in order to traverse, while starting point is less than or equal to end point
    - ie cstart<=cend, rstart<=rend
- (5) Create if else statements for direction traversal of matrix
    - For each direction, create for loop that allows traversal through either row or column
    - Push each element into the array while traversing
    - Decrement/increment the corresponding variable allowing for pushing (cstart, cend, rstart, rend)
    to account for decreasing sides
    - Increment direction variable

### Py Pseudocode
```py3
    def traversal(matrix):
        out=[]
        len(matrix) is 0 return out
        var start =(0,0)
        var end = (len(matrix)-1, len(matrix[0])-1)
        direction=0
        while(start<end):
            if direction is 0
                increment pointer from cstart to cend
                    add to output(matrix [rstart][i])
                increment rstart
                direction=direction+1%4
            elif direction is 1
                increment pointer from rstart to rend
                    add to output(matrix [i][cend])
                decrement cend
                direction=direction+1%4
            elif direction is 2
                decrement pointer from cend to cstart
                    add to output(matrix [rend][i])
                decrement rend
                direction=direction+1%4
            else
                decrement pointer from rend to rstart
                    add to output(matrix [cstart][i])
                increment cstart
                direction=direction+1%4
        return output
```

### My Implementation
- [Problem 2 Implementation](problem2.js)
### Solution
- [Problem 2 Official Solution](solution2.js)

## Intro Video Notes
- Walking through column or row in matrix is same as traversing through a single array
- How do you account for changing directions and decreasing sides
