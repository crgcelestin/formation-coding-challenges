function spiralTraversal(matrix) {
    //deal with 0*0 matrix (empty, no rows, columns)
    if (!matrix.length) return [];
    const output = [];
    /*
        problem states limit indices are inclusive
        initialize row start, end indices as well
        as column start, end indices
        in the prompt example,
        (rstart, cstart)=(0,0)
        (rend, cend)=(3,3)
        matrix.length=4
    */
    let rstart = 0, rend = matrix.length - 1;
    let cstart = 0, cend = matrix[0].length - 1;

    /*
        use 0 as across, 1 as down, 2 as across (backward) and 3 as up
    */
    let direction = 0
    //while there is points left in matrix (numbers in row, column)
    while (rstart <= rend && cstart <= cend) {
        //use different loop based on direction
        switch (direction) {
            //left to right
            case 0:
                for (let i = cstart; i <= cend; i++) {
                    output.push(matrix[rstart][i]);
                }
                rstart++;
                break;
            //down
            case 1:
                for (let i = rstart; i <= rend; i++) {
                    output.push(matrix[i][cend])
                }
                cend--
                break
            //right to left
            case 2:
                for (let i = cend; i >= cstart; i--) {
                    output.push(matrix[rend][i]);
                }
                rend--;
                break;
            //up
            case 3:
                for (let i = rend; i >= rstart; i--) {
                    output.push(matrix[i][cstart]);
                }
                cstart++;
                break;
        }
        /*
            move to next direction and wrap around
            if up was last performed, move across
        */
        direction = (direction + 1) % 4;
    }
    return output;
}


console.log(spiralTraversal(
    [
        [1, 2, 3, 4],
        [5, 6, 7, 8],
        [9, 10, 11, 12],
        [13, 14, 15, 16],
    ]
))
