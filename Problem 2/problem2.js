/*
attempting blind implementation
further thought process
*/
function traversal(matrix) {
    //checking for empty edge case
    if (!matrix.length) return []
    //initializing output array
    const output = []
    /*
        set parameters for matrix boundaries
        by initializing vars that store points on edges
    */
    let m_row_start = 0, m_row_end = matrix.length - 1
    let m_col_start = 0, m_col_end = matrix[0].length - 1
    //start direction for switch statements
    //default start is at 0,0 -> traverse to end (left to right) -> down -> right to left -> up -> left to right..
    let direction = 0
    //while loop, know that default row start is less than row end and col start is less than col end
    /*
        use switch statement to select one of many code blocks to be executed
        switch exp is eval'ed once
        value of exp is compared with values of each case
        if theres is a match an associated block is executed
        if there is no match default block is executed
    */
    while (m_row_start <= m_row_end & m_col_start <= m_col_end) {
        switch (direction) {
            //default start at case 0 which is left to right
            case 0:
                //moving forwards via columns
                for (let i = m_col_start; i <= m_col_end; i++) {
                    //pushing elements in that row to output array
                    output.push(matrix[m_row_start][i])
                }
                //increment as we are done with row
                m_row_start++;
                break;
            //need to go down
            case 1:
                //moving down rows
                for (let i = m_row_start; i <= m_row_end; i++) {
                    //pushing elements in column to output array
                    output.push(matrix[i][m_col_end])
                }
                //decrement as we are done with column
                //moving in as it relates to matrix
                m_col_end--;
                break;
            //right to left
            case 2:
                //moving backwards via columns
                for (let i = m_col_end; i >= m_col_start; i--) {
                    //push elements in row like forward
                    output.push(matrix[m_row_end][i])
                }
                //decrement as we are done with row
                m_row_end--;
                break;
            //up
            case 3:
                //moving up via rows
                for (let i = m_row_end; i >= m_row_start; i--) {
                    output.push(matrix[i][m_col_start])
                }
                m_col_start++;
                break;
        }
        //we will be incrementing to 3 and then when it gets to 4 -> goes back to 0 due to modulo
        direction = (direction + 1) % 4;
    }
    return output

}

console.log(traversal(
    [
        [9, 10, 11, 12],
        [5, 6, 7, 8],
        [13, 14, 15, 16],
        [1, 2, 3, 4],
    ]
))
